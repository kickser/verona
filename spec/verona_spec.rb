# frozen_string_literal: true

RSpec.describe Verona do
  it 'version must be 0.3.5' do
    expect(Verona::VERSION).to eq '0.3.5'
  end
end

# frozen_string_literal: true

module Verona
  module Errors
    class CredentialsError < StandardError
    end
  end
end

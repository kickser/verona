# frozen_string_literal: true

module Verona
  module ResponseGenerator
    begin
      RESPONSES = {
        auto_renewable: {
          samples: %w[cancelled_by_system_response cancelled_by_user_response
                      freetrial_success_response success_response],
          receipt: 'Verona::Subscription'
        },
        one_time: {
          samples: %w[success_response not_consumed_response],
          receipt: 'Verona::Receipt'
        }
      }.freeze

      RESPONSES.keys.each do |key|
        type = RESPONSES.dig(key)
        samples = type.dig(:samples)
        samples.each do |response|
          define_method("#{key}_#{response}") do
            receipt = type.dig(:receipt)
            Object.const_get(receipt).new(make_response(response, key))
          end
          define_method("#{key}_#{response}_raw") do
            make_response(response, key)
          end
        end
      end

      def self.make_response(response, key)
        name = "#{File.dirname __dir__}/verona/responses/#{key}/#{response}.json"
        JSON.parse(File.read(name)).tap do |data|
          success_response = response.include?('success_response')
          data['expiryTimeMillis'] = (Time.now.to_i + 3600) * 1000 if success_response && key == :auto_renewable
        end
      end

      extend self
    end
  end
end

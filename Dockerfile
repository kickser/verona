FROM ruby:2.5.5
LABEL maintainer="federicojosefarina@gmail.com"

RUN useradd -ms /bin/bash app
ENV RAILS_ROOT /home/app/verona

WORKDIR $RAILS_ROOT

# Add application files as my user
COPY --chown=app:app . $RAILS_ROOT

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN gem update --system
RUN gem install bundler

RUN bin/setup
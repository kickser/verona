Verona is a simple gem for verifying Google Play In-App Purchase receipts, and retrieving the information associated with receipt data.

There are two reasons why you should verify in-app purchase receipts on the server: First, it allows you to keep your own records of past purchases, which is useful for up-to-the-minute metrics and historical analysis. Second, server-side verification is one of the most reliable way to determine the authenticity of purchasing records.

> Verona is named for [Verona, Italy](http://en.wikipedia.org/wiki/Verona,_Italy). Also is a word game that refers to [Venice gem](https://github.com/nomad/venice), which validates purchases for Apple. Verona is the Venice classic football match.

## Installation

    $ gem install verona

## Usage

### Basic

#### Configure

```ruby
require 'verona'

Verona.configure do |config|
  # if true the logging will be performed via Rails.logger
  # if false (default) the logging will be peperformed via STDOUT
  config.use_rails_logger = true
  # path to your credentials json file
  config.credentials_file_path = 'some/path/to/credentials.json'
end
```

#### Verifying product purchase
```ruby
package = 'com.somepackage'
element_id = 'some_product_identifier'
purchase_token = 'some_hash_token'

begin
  receipt = Verona::Receipt.verify(package, element_id, purchase_token)
  puts "Valid receipt?: #{receipt.valid?}"
  puts receipt.to_h
rescue => e
  puts e.message
end
```

#### Verifying product subscription
```ruby
package = 'com.somepackage'
element_id = 'some_subscription_identifier'
purchase_token = 'some_hash_token'

begin
  subscription = Verona::Subscription.verify(package, element_id, purchase_token)
  puts "Valid receipt?: #{subscription.valid?}"
  puts subscription.to_h
rescue => e
  puts e.message
end
```

## Command Line Interface

### Dockerized environment (Or go directly to Run section)

#### Build and Up

```
make development_create
make development_up
```

#### Run

```
bin/console
```

## Test data and receipts

You can test with raw json playstore receipts or with Verona helper classes. You can see raw responses at lib/verona/responses folder too

### Auto renewable

Available responses: 
  - success_response
  - cancelled_by_system_response
  - cancelled_by_user_response
  - freetrial_response 

#### Examples

```

pry(main)> Verona::ResponseGenerator.auto_renewable_success_response_raw
{
  "kind"=>"androidpublisher#subscriptionPurchase",
  "startTimeMillis"=>1564710228371,
  "expiryTimeMillis"=>1567827815558,
  "autoRenewing"=>true,
  "priceCurrencyCode"=>"USD",
  "priceAmountMicros"=>"15990000",
  "countryCode"=>"AR",
  "paymentState"=>1,
  "orderId"=>"GPA.3395-7501-2591-55381..0"
}
```

```
pry(main)> Verona::ResponseGenerator.auto_renewable_success_response
  
  #<Verona::Subscription:0x0055bb1333ac60
  @auto_renewing=true,
  @cancel_reason=nil,
  @cancel_survey_result=nil,
  @country_code="AR",
  @developer_payload=nil,
  @email_address=nil,
  @expiry_time_millis=1567827815558,
  @family_name=nil,
  @given_name=nil,
  @kind="androidpublisher#subscriptionPurchase",
  @linked_purchase_token=nil,
  @order_id="GPA.3395-7501-2591-55381..0",
  @payment_state=1,
  @price_amount_micros="15990000",
  @price_currency_code="USD",
  @profile_id=nil,
  @profile_name=nil,
  @purchase_type=nil,
  @start_time_millis=1564710228371,
  @user_cancellation_time_millis=nil>

```

```
pry(main)> Verona::ResponseGenerator.auto_renewable_cancelled_by_user_response_raw
{
  "kind"=>"androidpublisher#subscriptionPurchase",
  "startTimeMillis"=>1565634217824,
  "expiryTimeMillis"=>1565634512947,
  "autoRenewing"=>false,
  "priceCurrencyCode"=>"USD",
  "priceAmountMicros"=>"15990000",
  "countryCode"=>"AR",
  "developerPayload"=>"",
  "cancelReason"=>0,
  "userCancellationTimeMillis"=>1565634281828,
  "cancelSurveyResult"=>
    { 
      "cancelSurveyReason"=>0,
      "userInputCancelReason"=>"Some reason"
    },
  "orderId"=>"GPA.3354-2318-1771-40811"
}
```


### One time purchases

Available responses: 
  - success_response


```
pry(main)> Verona::ResponseGenerator.one_time_success_response_raw
  
  #<Verona::Subscription:0x0055bb1333ac60
  @auto_renewing=true,
  @cancel_reason=nil,
  @cancel_survey_result=nil,
  @country_code="AR",
  @developer_payload=nil,
  @email_address=nil,
  @expiry_time_millis=1567827815558,
  @family_name=nil,
  @given_name=nil,
  @kind="androidpublisher#subscriptionPurchase",
  @linked_purchase_token=nil,
  @order_id="GPA.3395-7501-2591-55381..0",
  @payment_state=1,
  @price_amount_micros="15990000",
  @price_currency_code="USD",
  @profile_id=nil,
  @profile_name=nil,
  @purchase_type=nil,
  @start_time_millis=1564710228371,
  @user_cancellation_time_millis=nil>

```

## Creators

Federico Farina (https://github.com/fedefa) \
Juan Furattini ([@juanfurattini](https://twitter.com/frankancle))

## License

Venice is available under the MIT license. See the LICENSE file for more info.
